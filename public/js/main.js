var app = new Vue({
    el: ".container",
    created: function () {
        // Code appelé à la création de votre application
        console.log("Démarrage TODO-APP");
    },
    data: function () {
        return {
            todos: [],
            text: ""
        };
    },
    beforeMount: function () {
        // Code appelé juste avant l'affichage de votre application
        this.list();
    },
    methods: {
        list: function () {
            // Récupération des Todos
            console.log("Récupération Todo depuis le serveur");
            fetch("api/", {method: "GET", credentials: "same-origin"})
                .then(function (response) {
                    return response.json();
                })
                .then(function (response) {
                    app.todos = response;
                })
                .catch(function (error) {
                    console.log("Récupération impossible: " + error.message);
                });
        },
        add: function () {
            let formData = new FormData();
            formData.append("texte", app.text);

            fetch("api/add", {method: "POST", body: formData}).then(function () {
                app.text = ""; // On remet à Zéro l'input utilisateur
                app.list(); // On raffraîchit la liste.
            });
        },
        done: function (todo) {
            /*fetch(`api/done/${todo.id}`, {method: "PATCH"}).then(function () {
                app.list();
            });*/

            fetchThis("api/done/${todo.id}", "PATCH")
        },
        remove: function (todo) {
            this.$dialog
                .confirm('Confirmer la suppression')
                .then(function () {
                    console.log('Clicked on proceed');
                    fetch(`api/delete/${todo.id}`, {method: "DELETE"}).then(function () {
                        app.list();
                    })
                })
                .catch(function () {
                    console.log('Clicked on cancel');
                });
        }
    }
});
