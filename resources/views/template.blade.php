<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    @stack('styles')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- Ne fonctionne pas en local <link rel="stylesheet" href="{{ asset('css/all.css') }}" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/vuejs-dialog.min.css') }}">

    <!-- Scripts -->
    @stack('scripts')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.slim.min.js') }}" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/vuejs-dialog.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vuejs-dialog-mixin.min.js') }}"></script>
    <script>
        // Tell Vue to install the plugin.
        window.Vue.use(VuejsDialog.main.default)
    </script>
    <script type="text/javascript" src="{{ asset('js/utile.js') }}"></script>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="{{ route('todo.list') }}">Ma Todo List</a>
    <a class="navbar-brand" href="{{ route('todo.about') }}">A propos</a>
</nav>

@if(Session::has('message'))
    <p style="text-align: center" class="alert alert-danger">{{ Session::get('message') }}</p>
@endif

@yield('content')

</body>
</html>
