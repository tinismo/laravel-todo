@extends("template")

@section("title", "Todo List - Version VueJS")

@section("content")
    <div class="container">
        <div class="card">
            <div class="card-body">
                <!-- Action -->
                <div class="add">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Prendre une note…" v-on:keyup.enter="add"
                               v-model="text"/>
                    </div>
                </div>

                <!-- Liste des Todos -->
                <ul class="list-group pt-3">
                    <li class="list-group-item" v-for="todo in todos">
                        <span>@{{ todo.texte }}</span>
                        <div class="pull-right action">
                            <span v-if="todo.termine !== '1'" v-on:click="done(todo)" class="btn btn-success"><i
                                        class="fas fa-check"></i></span>
                            <span v-else v-on:click="remove(todo)" class="btn btn-danger"><i
                                        class="fas fa-trash"></i></span>
                        </div>
                    </li>
                    <li v-if="todos.length === 0" class="list-group-item text-center">C'est vide !</li>
                </ul>

            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endsection
