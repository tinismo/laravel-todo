<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todos;


class TodosController extends Controller{
    // C’est ici que seront nos méthodes
    public function liste(){
        return view("home", ["todos" => Todos::all()]);
    }

    public function saveTodo(Request $request){
        $texte = $request->input('texte');

        if($texte){
            $todo = new Todos();
            $todo->texte = $texte;
            $todo->termine = 0;
            $todo->save();
            return redirect()->route('todo.list');
        }
        return redirect()->route('todo.list')->with('message', "Veuillez entrer une note.");
    }

    public function markAsDone($id){
        $todo  = Todos::find($id);
        if($todo){
            $todo->termine = 1;
            $todo->save();
            return redirect()->route('todo.list');
        }
        return redirect()->route('todo.list')->with('message', "La note n'a pas pu être terminé.");
    }

    public function deleteTodo($id){
        $todo  = Todos::find($id);
        if($todo && $todo->termine == 1){
            $todo->delete();
            return redirect()->route('todo.list');
        }
        return redirect()->route('todo.list')->with('message', "La note n'a pas pu être supprimé.");
    }

    public function homevue(){
        return view("homevue");
    }
}


