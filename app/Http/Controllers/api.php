<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todos;


class api extends Controller
{
    //
    public function list(){
        return response()->json(Todos::all());
    }

    public function add(Request $request){
        $texte = $request->input('texte');

        if($texte){
            $todo = new Todos();
            $todo->texte = $texte;
            $todo->termine = 0;
            $todo->save();
            return response()->json(array("status" => 1));
        }
        return response()->json(array("status" => 0));
    }

    public function done($id){
        $todo  = Todos::find($id);
        if($todo){
            $todo->termine = 1;
            $todo->save();
            return response()->json(array("status" => 1));
        }
        return response()->json(array("status" => 0));
    }

    public function delete($id){
        $todo  = Todos::find($id);
        if($todo && $todo->termine == 1){
            $todo->delete();
            return response()->json(array("status" => 1));
        }
        return response()->json(array("status" => 0));
    }
}
